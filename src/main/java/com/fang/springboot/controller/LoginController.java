package com.fang.springboot.controller;

import com.fang.springboot.bean.User;
import com.fang.springboot.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(methods = {RequestMethod.POST,RequestMethod.GET})
public class LoginController {
    @Autowired
    UserDao userDao;

    @CrossOrigin
    @RequestMapping("/login")
    public String userLogin(@RequestBody User user) {

        String str = "error";
        int count = userDao.getUserByMassage(user.getUsername(), user.getPassword());
        System.out.println("User : " + user);

        if (count > 0) {
           str = "ok";
        }
       return str;
    }
}
