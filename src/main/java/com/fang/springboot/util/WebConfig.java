package com.fang.springboot.util;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//解决跨域问题
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                //允许访问路径
                .allowedOrigins("http://10.13.14.11:8080", "null")
                //允许访问方法
                .allowedMethods("POST", "GET", "PUT", "OPTIONS", "DELETE")
                //最大反应时间
                .maxAge(3600)
                //允许携带其他材料
                .allowCredentials(true);
    }
}