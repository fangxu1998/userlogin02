package com.fang.springboot.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class User {

    private int id;
    private String username;
    private String password;
    private String email;
    private String role;
    private boolean state;


}
