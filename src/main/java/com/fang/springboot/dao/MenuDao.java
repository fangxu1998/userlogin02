package com.fang.springboot.dao;

import com.fang.springboot.bean.MainMenu;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MenuDao {
    public List<MainMenu> getMainMenus();
}
